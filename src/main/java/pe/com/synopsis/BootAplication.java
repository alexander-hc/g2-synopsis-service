package pe.com.synopsis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BootAplication 
{
	public static void main(String[] args) 
	{
		SpringApplication.run(BootAplication.class, args);
	}
}
